use std::{
    collections::HashMap,
    fmt::{self, Display, Formatter},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FormatType
{
    Bold,
    Italic,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Heading
{
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParagraphPiece
{
    String(String),
    Format(Box<Paragraph>, FormatType),
    Heading(Box<Paragraph>, Heading),
    Up(Box<Paragraph>),
    Down(Box<Paragraph>),
    Link(String, Box<Paragraph>),
    InternalLink(String, Box<Paragraph>),
    Weblink(String, Box<Paragraph>),
    UnorderedList(Vec<Paragraph>),
    Line,
    Break,
    ArticleSeperator,
    SecondaryDate(Date, Date),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Paragraph
{
    pub inner: Vec<ParagraphPiece>,
}

#[derive(Debug, Clone)]
pub struct Entry
{
    pub title: String,
    pub content: Vec<Paragraph>,
}

#[derive(Debug, Clone)]
pub struct InternalLinkList
{
    pub en: HashMap<String, String>,
    pub de: HashMap<String, String>,
    pub eo: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub struct TripleEntry
{
    pub en: Entry,
    pub de: Entry,
    pub eo: Entry,
    pub date: FullDate,
}

#[derive(Debug, Clone)]
pub struct Blog
{
    pub entries: Vec<TripleEntry>,
    pub date: FullDate,
    pub internal_link_list: InternalLinkList,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Date
{
    pub year: u16,
    pub month: u8,
    pub day: u8,
    pub hour: u8,
    pub min: u8,
    pub sec: u8,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct FullDate
{
    pub published: Date,
    pub modified: Date,
}

impl From<Vec<ParagraphPiece>> for Paragraph
{
    fn from(inner: Vec<ParagraphPiece>) -> Self
    {
        Self { inner }
    }
}

impl From<ParagraphPiece> for Paragraph
{
    fn from(piece: ParagraphPiece) -> Self
    {
        Self { inner: vec![piece] }
    }
}

impl From<String> for Paragraph
{
    fn from(piece: String) -> Self
    {
        Self {
            inner: vec![ParagraphPiece::String(piece)],
        }
    }
}

impl From<&str> for Paragraph
{
    fn from(piece: &str) -> Self
    {
        Self {
            inner: vec![ParagraphPiece::String(piece.to_owned())],
        }
    }
}

impl Display for Date
{
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error>
    {
        write!(
            f,
            "{:04}-{:02}-{:02}T{:02}:{:02}:{:02}",
            self.year, self.month, self.day, self.hour, self.min, self.sec
        )
    }
}
