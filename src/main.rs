use std::env::set_var;

use anyhow::Error;
use zvavblog::gen_all;

fn main() -> Result<(), Error>
{
    set_var("RUST_BACKTRACE", "full");
    gen_all()
}
