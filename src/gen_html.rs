use std::{collections::HashMap, fs::File, io::Write, iter::once};

use anyhow::{Context, Error};

use crate::{
    lang::{Lang, RelativeLang},
    parse::title_to_path,
    types::{Blog, Date, Entry, FormatType, Heading, Paragraph, ParagraphPiece, TripleEntry},
};

impl Paragraph
{
    // Even though it's long, it's not complex.  False positive of a
    // pedantic lint.
    #[allow(clippy::too_many_lines)]
    fn to_html(
        &self,
        indent_count: usize,
        internal_link_list: &HashMap<String, String>,
        lang: Lang,
    ) -> Result<String, Error>
    {
        let mut rv = String::new();

        let indent = "  ".repeat(indent_count);

        for piece in &self.inner
        {
            rv.push_str(&match piece
            {
                ParagraphPiece::String(x) => x.to_string(),
                ParagraphPiece::Format(x, FormatType::Bold) =>
                {
                    format!(
                        "<strong>{}</strong>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Format(x, FormatType::Italic) =>
                {
                    format!(
                        "<em>{}</em>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H1) =>
                {
                    format!(
                        "  <h1>{}</h1>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H2) =>
                {
                    format!(
                        "  <h2>{}</h2>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H3) =>
                {
                    format!(
                        "  <h3>{}</h3>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H4) =>
                {
                    format!(
                        "  <h4>{}</h4>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H5) =>
                {
                    format!(
                        "  <h5>{}</h5>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Heading(x, Heading::H6) =>
                {
                    format!(
                        "  <h6>{}</h6>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Up(x) =>
                {
                    format!(
                        "<sup>{}</sup>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Down(x) =>
                {
                    format!(
                        "<sub>{}</sub>",
                        x.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::Link(url, text) | ParagraphPiece::Weblink(url, text) =>
                {
                    format!(
                        "<a href=\"{url}\">{}</a>",
                        text.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                ParagraphPiece::InternalLink(url, text) =>
                {
                    format!(
                        "<a href=\"{}.html\">{}</a>",
                        internal_link_list
                            .get(url)
                            .context("Internal link to nonexistent site.")?,
                        text.to_html(indent_count, internal_link_list, lang)?
                    )
                }
                // This is a performance lint.  While it is
                // technically correct – in that the code is slow how
                // it's written right now – I still disabled it,
                // because a) I don't really care about performance
                // here and b) I think it looks much less readable if
                // I were to do what the lint is recommending.
                #[allow(clippy::format_collect)]
                ParagraphPiece::UnorderedList(content) => format!(
                    "{}{indent}  <ul>{}\n      {indent}</ul>{}",
                    if indent_count == 0 { "" } else { "\n      " },
                    content
                        .iter()
                        .map(|x| x.to_html(indent_count + 2, internal_link_list, lang))
                        .collect::<Result<Vec<_>, _>>()?
                        .into_iter()
                        .map(|x| format!("\n        {indent}<li>{x}</li>"))
                        .collect::<String>(),
                    if indent_count == 0
                    {
                        String::new()
                    }
                    else
                    {
                        format!("\n{indent}      ")
                    }
                ),
                ParagraphPiece::Line => format!("{indent}  <hr>"),
                ParagraphPiece::Break => format!("{indent}  <br>"),
                ParagraphPiece::ArticleSeperator => "</article>\n    <article>".to_string(),
                ParagraphPiece::SecondaryDate(published, modified) =>
                {
                    format!(
                        r#"  <div class="secondarydate">
          <small>
            {}: {published}
            <br>
            {}: {modified}
          </small>
        </div>"#,
                        lang.published(),
                        lang.modified()
                    )
                }
            });
        }

        Ok(rv)
    }
}

impl Entry
{
    pub fn gen_article(
        &self,
        default_indent_count: usize,
        internal_link_list: &HashMap<String, String>,
        lang: Lang,
    ) -> Result<String, Error>
    {
        let default_indent = "  ".repeat(default_indent_count);

        Ok(self
            .content
            .iter()
            .map(|paragraph| {
                paragraph
                    .to_html(default_indent_count, internal_link_list, lang)
                    .map(|s| {
                        (
                            s,
                            paragraph.inner.iter().all(|piece| {
                                matches!(
                                    piece,
                                    ParagraphPiece::Heading(_, _)
                                        | ParagraphPiece::UnorderedList(_)
                                        | ParagraphPiece::Break
                                        | ParagraphPiece::Line
                                        | ParagraphPiece::ArticleSeperator
                                        | ParagraphPiece::SecondaryDate(_, _)
                                )
                            }),
                        )
                    })
            })
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .map(|(paragraph, header)| {
                if header
                {
                    format!("{default_indent}    {paragraph}\n")
                }
                else
                {
                    format!("{default_indent}      <p>{paragraph}</p>\n")
                }
            })
            .collect::<String>())
    }

    // False positive – I think it's better readable this way than in
    // any other one.
    #[allow(clippy::too_many_arguments)]
    pub fn to_html(
        &self,
        other_lang1: &str,
        other_lang2: &str,
        blog: &str,
        startpage: &str,
        archive: &str,
        contact: &str,
        legal: &str,
        internal_link_list: &HashMap<String, String>,
        date_published: Date,
        date_modified: Date,
        lang: Lang,
    ) -> Result<String, Error>
    {
        Ok(format!(
            r#"<!DOCTYPE html>
<html lang="{}">
  <head>
    <meta charset="UTF-8">
    <title>{}</title>
    <link rel="stylesheet" href="/main.css">
    <link rel="icon" href="/icon.svg">
    <link rel="alternate" type="application/atom+xml" href="/feed.atom">
  </head>
  <body>
    <header>
      <h1>{}</h1>
      <div id="primarydate">
        <small>
          {}: {date_published}
          <br>
          {}: {date_modified}
        </small>
      </div>
      <nav>
        <div id="other_langs">
          {other_lang1}
          {other_lang2}
        </div>
        <div id="links">
          {blog}
          <br>
          {startpage}
          <br>
          {archive}
          <br>
          {contact}
          <br>
          <a href="/feed.atom">RSS</a>
        </div>
      </nav>
    </header>
    <main>
      <article>
{}      </article>
    </main>
    <footer>
      <hr>
      {legal}
      <p>Ceterum censeo monarchiam delendam.</p>
    </footer>
  </body>
</html>
"#,
            lang.lang_code(),
            self.title,
            self.title,
            lang.published(),
            lang.modified(),
            self.gen_article(1, internal_link_list, lang)?
        ))
    }
}

impl TripleEntry
{
    pub fn to_html(&self, blog: &Blog) -> Result<(), Error>
    {
        let file_link = Lang::iter()
            .map(|lang| {
                let title = title_to_path(&lang.to_entry(self).title);

                let (url, file) = if self.en.title == Lang::En.blog_title()
                {
                    (String::new(), String::from("index"))
                }
                else
                {
                    (format!("/{title}.html"), title)
                };

                let link_to = format!(
                    "<a href=\"https://{}blog.zvavybir.eu{url}\">{}</a>",
                    lang.url_prefix(),
                    lang.flag()
                );

                (file, link_to)
            })
            .collect::<Vec<_>>();

        for lang in Lang::iter()
        {
            let blog_link = format!(r#"<a href="/">{}</a>"#, lang.blog());
            let startpage = format!(
                r#"<a href="https://{}zvavybir.eu/">{}</a>"#,
                lang.url_prefix(),
                lang.startpage()
            );
            let archive = format!(
                r#"<a href="{}.html">{}</a>"#,
                lang.archive().to_lowercase(),
                lang.archive()
            );
            let contact = format!(r#"<a href="/contact">{}</a>"#, lang.contact());
            let legal = lang.legal();

            write!(
                File::create(format!(
                    "output/{}/{}.html",
                    lang.lang_code(),
                    file_link[lang.from_relative(RelativeLang::Same).to_index()].0
                ))
                .with_context(|| format!("Couldn't open the {} page", lang.lang_name()))?,
                "{}",
                lang.to_entry(self)
                    .to_html(
                        &file_link[lang.from_relative(RelativeLang::Other1).to_index()].1,
                        &file_link[lang.from_relative(RelativeLang::Other2).to_index()].1,
                        &blog_link,
                        &startpage,
                        &archive,
                        &contact,
                        legal,
                        lang.to_internal_linked_list(blog),
                        self.date.published,
                        self.date.modified,
                        lang
                    )
                    .with_context(|| format!("Couldn't generate the {} page", lang.lang_name()))?,
            )
            .with_context(|| format!("Couldn't write the {} page", lang.lang_name()))?;
        }

        Ok(())
    }
}

fn gen_archive(blog: &mut Blog)
{
    let [archive_en, archive_de, archive_eo] = Lang::iter()
        .map(|lang| {
            let mut archive = vec![];
            for (i, triple_entry) in blog.entries.iter().enumerate()
            {
                if i != 0
                {
                    archive.push(ParagraphPiece::Break);
                }

                archive.push(ParagraphPiece::Link(
                    format!("{}.html", title_to_path(&lang.to_entry(triple_entry).title)),
                    Box::new(lang.to_entry(triple_entry).title.clone().into()),
                ));
            }

            let archive = vec![
                Paragraph::from(vec![
                    ParagraphPiece::String(lang.archive_text_part1()),
                    ParagraphPiece::Link(
                        "/sources.zip".to_string(),
                        Box::new(Paragraph::from(lang.archive_text_part2())),
                    ),
                    ParagraphPiece::String(lang.archive_text_part3()),
                    ParagraphPiece::Link(
                        "/html.zip".to_string(),
                        Box::new(Paragraph::from(lang.archive_text_part4())),
                    ),
                    ParagraphPiece::String(".".to_string()),
                ]),
                Paragraph::from(archive),
            ];

            archive
        })
        .collect::<Vec<_>>()
        .try_into()
        .expect("Bug in zvavblog");

    blog.entries.push(TripleEntry {
        en: Entry {
            title: Lang::En.archive().to_string(),
            content: archive_en,
        },
        de: Entry {
            title: Lang::De.archive().to_string(),
            content: archive_de,
        },
        eo: Entry {
            title: Lang::Eo.archive().to_string(),
            content: archive_eo,
        },
        date: blog.date,
    });
}

fn gen_index(blog: &mut Blog)
{
    let [index_en, index_de, index_eo] = Lang::iter()
        .map(|lang| {
            blog.entries
                .iter()
                .map(move |triple_entry| (lang.to_entry(triple_entry), triple_entry.date))
                .map(|(val, date)| {
                    once(Paragraph::from(ParagraphPiece::Heading(
                        Box::new(Paragraph::from(val.title.clone())),
                        Heading::H2,
                    )))
                    .chain(once(Paragraph::from(ParagraphPiece::SecondaryDate(
                        date.published,
                        date.modified,
                    ))))
                    .chain(val.content.iter().cloned())
                    .collect::<Vec<_>>()
                    .into_iter()
                })
                .intersperse(
                    vec![
                        Paragraph::from(ParagraphPiece::Line),
                        Paragraph::from(ParagraphPiece::ArticleSeperator),
                    ]
                    .into_iter(),
                )
                .flatten()
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
        .try_into()
        .expect("Bug in zvavblog");

    blog.entries.push(TripleEntry {
        en: Entry {
            title: Lang::En.blog_title().to_string(),
            content: index_en,
        },
        de: Entry {
            title: Lang::De.blog_title().to_string(),
            content: index_de,
        },
        eo: Entry {
            title: Lang::Eo.blog_title().to_string(),
            content: index_eo,
        },
        date: blog.date,
    });
}

fn gen_html(blog: &Blog) -> Result<(), Error>
{
    blog.entries
        .iter()
        .try_for_each(|x| {
            x.to_html(blog)
                .with_context(|| format!("Couldn't write file: {x:?}"))
        })
        .context("There was a problem generating all HTML files")
}

pub fn gen_all(mut blog: Blog) -> Result<(), Error>
{
    gen_archive(&mut blog);
    gen_index(&mut blog);
    gen_html(&blog).context("Couldn't generate all HTML files")?;

    Ok(())
}
