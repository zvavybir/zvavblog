use crate::types::{FullDate, TripleEntry};

impl FullDate
{
    pub fn get_blog_dates(triple_entries: &[TripleEntry]) -> Self
    {
        Self {
            published: triple_entries
                .iter()
                .map(|triple_entry| triple_entry.date.published)
                .min()
                .expect("Bug in zvavblog"),
            modified: triple_entries
                .iter()
                .map(|triple_entry| triple_entry.date.modified)
                .max()
                .expect("Bug in zvavblog"),
        }
    }
}
