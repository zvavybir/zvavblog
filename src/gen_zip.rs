use std::{fs::copy, process::Command};

use anyhow::{Context, Error};

pub fn gen_all() -> Result<(), Error>
{
    Command::new("sh")
        .args(["-c", "zip -FS output/en/html.zip output/*/*.html"])
        .status()
        .context("Coulnd't zip the HTML files")?;
    copy("output/en/html.zip", "output/de/html.zip")
        .context("Couldn't copy html zip file to the German site")?;
    copy("output/en/html.zip", "output/eo/html.zip")
        .context("Couldn't copy html zip file to the Esperanto site")?;

    Command::new("sh")
        .args(["-c", "zip -FS output/en/sources.zip sources/*.txt"])
        .status()
        .context("Coulnd't zip the source files")?;
    copy("output/en/sources.zip", "output/de/sources.zip")
        .context("Couldn't copy source zip file to the German site")?;
    copy("output/en/sources.zip", "output/eo/sources.zip")
        .context("Couldn't copy source zip file to the Esperanto site")?;

    Ok(())
}
