use std::fs::create_dir_all;

use anyhow::{anyhow, Context, Error};

use crate::{gen_atom, gen_html, gen_zip, types::Blog};

/// Generates everything
///
/// Parses the sources and outputs the HTML.
///
/// # Errors
/// Returns an error if there was any.
pub fn gen_all() -> Result<(), Error>
{
    create_dir_all("output/en").context("Couldn't create the English output directory")?;
    create_dir_all("output/de").context("Couldn't create the German output directory")?;
    create_dir_all("output/eo").context("Couldn't create the Esperanto output directory")?;

    let blog = Blog::parse_blog().context("Couldn't parse files")?;

    if blog.entries.is_empty()
    {
        return Err(anyhow!("The blog is empty"));
    }

    gen_atom::gen_all(&blog).context("Couldn't generate Atom")?;
    gen_html::gen_all(blog).context("Couldn't generate HTML")?;
    gen_zip::gen_all().context("Couldn't generate ZIP files")?;
    Ok(())
}
