use std::{
    cmp::Ordering,
    collections::HashMap,
    fs::{read_dir, read_to_string},
    mem::take,
};

use anyhow::{anyhow, bail, Context, Error};

use crate::{
    lang::Lang,
    types::{
        Blog, Date, Entry, FormatType, FullDate, Heading, InternalLinkList, Paragraph,
        ParagraphPiece, TripleEntry,
    },
};

fn parse_piece<F>(
    num: usize,
    command: &str,
    pieces: &[String],
    f: F,
) -> Result<ParagraphPiece, Error>
where
    F: FnOnce() -> Result<ParagraphPiece, Error>,
{
    match pieces.len().cmp(&num)
    {
        Ordering::Less => Err(anyhow!(
            "Too few pieces ({} instead of {num}) in {command} command",
            pieces.len()
        )),
        Ordering::Greater => Err(anyhow!(
            "Too many pieces ({} instead of {num}) in {command} command",
            pieces.len()
        )),
        Ordering::Equal => f(),
    }
}

impl ParagraphPiece
{
    fn parse_command(pieces: &[String]) -> Result<Self, Error>
    {
        match pieces[0].as_str()
        {
            "b" | "bold" => parse_piece(2, "bold formatting", pieces, || {
                Ok(Self::Format(
                    Box::new(Paragraph::new(&pieces[1])?),
                    FormatType::Bold,
                ))
            }),
            "i" | "italic" => parse_piece(2, "italic formatting", pieces, || {
                Ok(Self::Format(
                    Box::new(Paragraph::new(&pieces[1])?),
                    FormatType::Italic,
                ))
            }),
            "h1" => parse_piece(2, "h1", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H1,
                ))
            }),
            "h2" => parse_piece(2, "h2", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H2,
                ))
            }),
            "h3" => parse_piece(2, "h3", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H3,
                ))
            }),
            "h4" => parse_piece(2, "h4", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H4,
                ))
            }),
            "h5" => parse_piece(2, "h5", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H5,
                ))
            }),
            "h6" => parse_piece(2, "h6", pieces, || {
                Ok(Self::Heading(
                    Box::new(Paragraph::new(&pieces[1])?),
                    Heading::H6,
                ))
            }),
            "up" => parse_piece(2, "up", pieces, || {
                Ok(Self::Up(Box::new(Paragraph::new(&pieces[1])?)))
            }),
            "down" => parse_piece(2, "down", pieces, || {
                Ok(Self::Down(Box::new(Paragraph::new(&pieces[1])?)))
            }),
            "l" | "link" => parse_piece(3, "link", pieces, || {
                Ok(Self::Link(
                    pieces[1].clone(),
                    Box::new(Paragraph::new(&pieces[2])?),
                ))
            }),
            "wl" | "weblink" => parse_piece(3, "website link", pieces, || {
                Ok(Self::Weblink(
                    pieces[1].clone(),
                    Box::new(Paragraph::new(&pieces[2])?),
                ))
            }),
            "il" | "internallink" => parse_piece(3, "internal link", pieces, || {
                Ok(Self::InternalLink(
                    pieces[1].clone(),
                    Box::new(Paragraph::new(&pieces[2])?),
                ))
            }),
            "bo" | "braceopen" => parse_piece(1, "brace open", pieces, || {
                Ok(Self::String("{".to_string()))
            }),
            "bc" | "braceclose" => parse_piece(1, "brace close", pieces, || {
                Ok(Self::String("}".to_string()))
            }),
            "v" | "verticalbar" => parse_piece(1, "vertical bar", pieces, || {
                Ok(Self::String("|".to_string()))
            }),
            "ul" => Ok(Self::UnorderedList(
                pieces
                    .iter()
                    .skip(1)
                    .map(|x| Paragraph::new(x))
                    .collect::<Result<_, _>>()?,
            )),
            e => Err(anyhow!("Unknown command {:?}", e)),
        }
    }
}

impl Paragraph
{
    fn new(s: &str) -> Result<Self, Error>
    {
        let mut depth = 0;
        let mut rv = vec![];
        let mut pieces = vec![];
        let mut piece = String::new();

        for c in s.chars()
        {
            match (c, depth)
            {
                ('{', 0) =>
                {
                    depth = 1;
                    if !piece.is_empty()
                    {
                        rv.push(ParagraphPiece::String(take(&mut piece)));
                    }
                }
                ('{', _) =>
                {
                    piece.push(c);
                    depth += 1;
                }
                ('|', 1) => pieces.push(take(&mut piece)),
                ('|', 0) => return Err(anyhow!("Delimiter outside of command: {s:?}")),
                ('}', 0) => return Err(anyhow!("Mismatched braces (missing open)")),
                ('}', 1) =>
                {
                    pieces.push(take(&mut piece));
                    rv.push(ParagraphPiece::parse_command(&pieces)?);
                    pieces.clear();
                    depth = 0;
                }
                ('}', _) =>
                {
                    piece.push(c);
                    depth -= 1;
                }
                (c, _) => piece.push(c),
            }
        }

        if !piece.is_empty()
        {
            rv.push(ParagraphPiece::String(take(&mut piece)));
        }

        if depth != 0
        {
            return Err(anyhow!("Mismatched braces (closing missing)"));
        }

        Ok(Self::from(rv))
    }
}

fn parse_date(s: &str) -> Result<Date, Error>
{
    let splitted = s.split(' ').collect::<Vec<_>>();

    if splitted.len() != 6
    {
        bail!("Wrong number of numbers in date");
    }

    let date = Date {
        year: splitted[0].parse().context("Couldn't parse year in date")?,
        month: splitted[1]
            .parse()
            .context("Couldn't parse month in date")?,
        day: splitted[2].parse().context("Couldn't parse day in date")?,
        hour: splitted[3].parse().context("Couldn't parse hour in date")?,
        min: splitted[4]
            .parse()
            .context("Couldn't parse minute in date")?,
        sec: splitted[5]
            .parse()
            .context("Couldn't parse second in date")?,
    };

    if date.year < 1900 || date.year > 2100
    {
        bail!("Are you sure the year ({}) is correct?", date.year);
    }
    if !(1..=12).contains(&date.month)
        || !(1..=31).contains(&date.day)
        || !(0..=23).contains(&date.hour)
        || !(0..=59).contains(&date.min)
        || !(0..=60).contains(&date.sec)
    {
        bail!("Invalid time")
    }

    Ok(date)
}

impl TripleEntry
{
    fn new(s: &str) -> Result<Self, Error>
    {
        let mut title = None;
        let mut all = vec![];
        let mut single = vec![];

        let mut lines = s.lines();
        let date_published = parse_date(
            lines
                .next()
                .context("Every article needs a publication date")?,
        )
        .context("Problem parsing publication date")?;
        let date_modified = parse_date(
            lines
                .next()
                .context("Every article needs a modification date")?,
        )
        .context("Problem parsing modification date")?;

        for (i, line) in lines.enumerate()
        {
            if line.starts_with("Title: ")
            {
                if i != 0
                {
                    all.push((title.context("Bug in zvavblog")?, take(&mut single)));
                }
                title = Some(line.strip_prefix("Title: ").context("Bug in zvavblog")?);
            }
            else if !line.is_empty()
            {
                single.push(Paragraph::new(line)?);
            }
        }
        all.push((title.context("Bug in zvavblog")?, take(&mut single)));

        let [en, de, eo] = all
            .into_iter()
            .map(|(title, content)| Entry {
                title: title.to_owned(),
                content,
            })
            .collect::<Vec<_>>()
            .try_into()
            .map_err(|vec| anyhow!("{vec:?}"))
            .context("Always need three languages")?;

        Ok(Self {
            en,
            de,
            eo,
            date: FullDate {
                published: date_published,
                modified: date_modified,
            },
        })
    }
}

impl Blog
{
    pub fn parse_blog() -> Result<Self, Error>
    {
        let mut paths = vec![];

        for file in read_dir("sources").context("Couldn't open source directory")?
        {
            let file = file.context("Problem iterating over file")?;

            if file
                .file_type()
                .context("Couldn't get file type")?
                .is_file()
            {
                let path = file.path();

                if path
                    .extension()
                    .map_or(false, |extension| extension.eq_ignore_ascii_case("txt"))
                {
                    paths.push(path);
                }
            }
            else
            {
                eprintln!("File is not regular file: {file:?}");
            }
        }

        paths.sort_unstable_by(|a, b| b.cmp(a));

        let mut rv = vec![];
        for path in paths
        {
            rv.push(TripleEntry::new(
                &read_to_string(&path).with_context(|| format!("Couldn't read file: {path:?}"))?,
            )?);
        }

        let date = FullDate::get_blog_dates(&rv);

        let [internal_link_list_en, internal_link_list_de, internal_link_list_eo] = Lang::iter()
            .map(|lang| {
                rv.iter()
                    .map(|triple_entry| {
                        (
                            title_to_path(&triple_entry.en.title),
                            title_to_path(&lang.to_entry(triple_entry).title),
                        )
                    })
                    .chain(vec![(
                        Lang::En.archive().to_lowercase(),
                        lang.archive().to_lowercase(),
                    )])
                    .collect::<HashMap<_, _>>()
            })
            .collect::<Vec<_>>()
            .try_into()
            .expect("Bug in zvavblog");

        Ok(Self {
            entries: rv,
            date,
            internal_link_list: InternalLinkList {
                en: internal_link_list_en,
                de: internal_link_list_de,
                eo: internal_link_list_eo,
            },
        })
    }
}

pub fn title_to_path(title: &str) -> String
{
    title
        .chars()
        .map(|c| {
            if c.is_whitespace()
            {
                '_'
            }
            else
            {
                c.to_ascii_lowercase()
            }
        })
        .filter(|c| matches!(c, 'a'..='z' | '0'..='9' | '.' | '_'))
        .collect()
}

#[cfg(test)]
mod tests
{
    use crate::types::{FormatType, Paragraph, ParagraphPiece};

    #[test]
    fn paragraph_parse()
    {
        let tests = vec![
            ("abc", Paragraph::from("abc")),
            ("", Paragraph { inner: vec![] }),
            (
                "a{b|test}bc",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::String("a".to_owned()),
                        ParagraphPiece::Format(Box::new(Paragraph::from("test")), FormatType::Bold),
                        ParagraphPiece::String("bc".to_owned()),
                    ],
                },
            ),
            (
                "{b|test}bc",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::Format(Box::new(Paragraph::from("test")), FormatType::Bold),
                        ParagraphPiece::String("bc".to_owned()),
                    ],
                },
            ),
            (
                "a{b|test}",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::String("a".to_owned()),
                        ParagraphPiece::Format(Box::new(Paragraph::from("test")), FormatType::Bold),
                    ],
                },
            ),
            (
                "a{b|}bc",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::String("a".to_owned()),
                        ParagraphPiece::Format(
                            Box::new(Paragraph { inner: vec![] }),
                            FormatType::Bold,
                        ),
                        ParagraphPiece::String("bc".to_owned()),
                    ],
                },
            ),
            (
                "a{b|test}b{i|TEST}c",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::String("a".to_owned()),
                        ParagraphPiece::Format(Box::new(Paragraph::from("test")), FormatType::Bold),
                        ParagraphPiece::String("b".to_owned()),
                        ParagraphPiece::Format(
                            Box::new(Paragraph::from("TEST")),
                            FormatType::Italic,
                        ),
                        ParagraphPiece::String("c".to_owned()),
                    ],
                },
            ),
            (
                "a{b|t{i|e}st}bc",
                Paragraph {
                    inner: vec![
                        ParagraphPiece::String("a".to_owned()),
                        ParagraphPiece::Format(
                            Box::new(Paragraph {
                                inner: vec![
                                    ParagraphPiece::String("t".to_owned()),
                                    ParagraphPiece::Format(
                                        Box::new(Paragraph::from("e")),
                                        FormatType::Italic,
                                    ),
                                    ParagraphPiece::String("st".to_owned()),
                                ],
                            }),
                            FormatType::Bold,
                        ),
                        ParagraphPiece::String("bc".to_owned()),
                    ],
                },
            ),
        ];

        for (input, output) in tests
        {
            assert_eq!(Paragraph::new(input).unwrap(), output);
        }
    }
}
