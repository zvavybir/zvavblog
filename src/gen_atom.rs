use std::{collections::HashMap, fs::File, io::Write};

use anyhow::{Context, Error};

use crate::{
    lang::Lang,
    parse::title_to_path,
    types::{Blog, Entry, FullDate},
};

impl Entry
{
    // I have to work on this, but this is the most readable right
    // now.
    #[allow(clippy::too_many_arguments)]
    fn to_atom(
        &self,
        internal_link_list: &HashMap<String, String>,
        title: &str,
        date: FullDate,
        lang: Lang,
    ) -> Result<String, Error>
    {
        Ok(format!(
            r#"  <entry>
    <title>{}</title>
    <link rel="alternate" type="text/html" href="https://{}blog.zvavybir.eu/{}.html"/>
    <id>https://{}.blog.zvavybir.eu/blog_id/{}</id>
    <published>{}</published>
    <updated>{}</updated>
    <content type="xhtml">
      <div xmlns="http://www.w3.org/1999/xhtml">
{}    </div>
    </content>
    <author>
      <name>Matthias Kaak</name>
      <uri>{}</uri>
      <email>zvavybir@zvavybir.eu</email>
    </author>
  </entry>"#,
            self.title,
            lang.url_prefix(),
            title_to_path(title),
            lang.lang_code(),
            date.published,
            date.published,
            date.modified,
            self.gen_article(1, internal_link_list, lang)?,
            lang.homepage()
        ))
    }
}

pub fn gen_all(blog: &Blog) -> Result<(), Error>
{
    for lang in Lang::iter()
    {
        writeln!(
            File::create(format!("output/{}/feed.atom", lang.lang_code()))
                .with_context(|| format!("Couldn't open {} Atom feed", lang.lang_name()))?,
            r#"<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="{}">

  <title>{}</title>
  <subtitle>{}</subtitle>
  <link href="https://{}blog.zvavybir.eu/feed.atom" rel="self" hreflang="{}" />
  <link href="https://{}blog.zvavybir.eu/" rel="alternate" type="text/html" hreflang="{}" />
  <id>https://{}blog.zvavybir.eu/</id>
  <published>{}</published>
  <updated>{}</updated>

{}
</feed>"#,
            lang.lang_code(),
            lang.blog_title(),
            lang.subtitle(),
            lang.url_prefix(),
            lang.lang_code(),
            lang.url_prefix(),
            lang.lang_code(),
            lang.url_prefix(),
            blog.date.published,
            blog.date.modified,
            blog.entries
                .iter()
                .map(|triple_entry| {
                    let entry = lang.to_entry(triple_entry);
                    entry.to_atom(
                        lang.to_internal_linked_list(blog),
                        &entry.title,
                        triple_entry.date,
                        lang,
                    )
                })
                .collect::<Result<Vec<_>, _>>()
                .with_context(|| format!("Couldn't generate {} Atom feed", lang.lang_name()))?
                .join("\n\n")
        )
        .with_context(|| format!("Couldn't write {} Atom feed", lang.lang_name()))?;
    }

    Ok(())
}
