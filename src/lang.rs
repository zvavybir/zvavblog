use std::collections::HashMap;

use crate::types::{Blog, Entry, TripleEntry};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Lang
{
    En,
    De,
    Eo,
}

#[derive(Clone, Copy)]
pub enum RelativeLang
{
    Same,
    Other1,
    Other2,
}

impl Lang
{
    pub const fn lang_code(self) -> &'static str
    {
        match self
        {
            Self::En => "en",
            Self::De => "de",
            Self::Eo => "eo",
        }
    }

    pub const fn url_prefix(self) -> &'static str
    {
        match self
        {
            Self::En => "",
            Self::De => "de.",
            Self::Eo => "eo.",
        }
    }

    pub const fn lang_name(self) -> &'static str
    {
        match self
        {
            Self::En => "English",
            Self::De => "German",
            Self::Eo => "Esperanto",
        }
    }

    pub const fn blog_title(self) -> &'static str
    {
        match self
        {
            Self::En => "zvavybir's Blog",
            Self::De => "zvavybirs Blog",
            Self::Eo => "Blogo de zvavybir",
        }
    }

    pub const fn published(self) -> &'static str
    {
        match self
        {
            Self::En => "Published",
            Self::De => "Veröffentlicht",
            Self::Eo => "Publikigita",
        }
    }

    pub const fn modified(self) -> &'static str
    {
        match self
        {
            Self::En => "Modified",
            Self::De => "Verändert",
            Self::Eo => "Modifikita",
        }
    }

    pub const fn homepage(self) -> &'static str
    {
        match self
        {
            Self::En => "https://zvavybir.eu/",
            Self::De => "https://de.zvavybir.eu/",
            Self::Eo => "https://eo.zvavybir.eu/",
        }
    }

    pub const fn subtitle(self) -> &'static str
    {
        match self
        {
            Self::En => "I'm writin' about stuff",
            Self::De => "Ich schreib' Zeug",
            Self::Eo => "Mi skribas pri aĵojn",
        }
    }

    pub const fn flag(self) -> &'static str
    {
        match self
        {
            Self::En => "\u{1F1FA}\u{1F1F8}",
            Self::De => "\u{1F1E9}\u{1F1EA}",
            Self::Eo => r#"<img width="20" src="/eo_flag.svg" alt="EO">"#,
        }
    }

    pub const fn blog(self) -> &'static str
    {
        match self
        {
            Self::En | Self::De => "Blog",
            Self::Eo => "Blogo",
        }
    }

    pub const fn startpage(self) -> &'static str
    {
        match self
        {
            Self::En => "Startpage",
            Self::De => "Startseite",
            Self::Eo => "Ĉefpago",
        }
    }

    pub const fn archive(self) -> &'static str
    {
        match self
        {
            Self::En => "Archive",
            Self::De => "Archiv",
            Self::Eo => "Arkivo",
        }
    }

    pub const fn contact(self) -> &'static str
    {
        match self
        {
            Self::En => "Contact",
            Self::De => "Kontakt",
            Self::Eo => "Kontakto",
        }
    }

    pub const fn legal(self) -> &'static str
    {
        match self
        {
            Self::En =>
            {
                "<p>Legal: This site is free.  Code ist under the marked free license, the rest is \
		 under <a href=\"https://creativecommons.org/publicdomain/zero/1.0/legalcode\">CC0</a>.</p>"
            }
            Self::De =>
            {
                "<p>Rechtliches: Diese Seite ist frei.  Alle Programme sind unter der jeweils \
		 markierten freien Lizenz und der Rest unter \
		 <a href=\"https://creativecommons.org/publicdomain/zero/1.0/legalcode.de\">CC0</a>.</p>"
            }
            Self::Eo =>
            {
                "<p>Leĝaĵoj: Tiu ĉi retejo estas libera.  Ĉiu programo estas \
		 licencita je la idikita libera licenco kaj la resto je \
		 <a href=\"https://creativecommons.org/publicdomain/zero/1.0/legalcode\">CC0</a>.</p>"
            }
        }
    }

    pub fn archive_text_part1(self) -> String
    {
        match self
        {
            Self::En => "You can ",
            Self::De => "Du kannst ",
            Self::Eo => "Vi povas ",
        }
        .to_string()
    }

    pub const fn archive_text_part2(self) -> &'static str
    {
        match self
        {
            Self::En => "download the text sources here",
            Self::De => "die Textquellen hier herunterladen",
            Self::Eo => "elŝuti la tekst-fontojn ĉi tie",
        }
    }

    pub fn archive_text_part3(self) -> String
    {
        match self
        {
            Self::En => " and ",
            Self::De => " und ",
            Self::Eo => " kaj ",
        }
        .to_string()
    }

    pub const fn archive_text_part4(self) -> &'static str
    {
        match self
        {
            Self::En => "all rendered HTML files here",
            Self::De => "alle gerenderten HTML-Dateien hier",
            Self::Eo => "ĉiujn de la bildigitajn HTML-dosierojn ĉi tie",
        }
    }

    pub const fn to_entry(self, triple_entry: &TripleEntry) -> &Entry
    {
        match self
        {
            Self::En => &triple_entry.en,
            Self::De => &triple_entry.de,
            Self::Eo => &triple_entry.eo,
        }
    }

    pub const fn to_internal_linked_list(self, blog: &Blog) -> &HashMap<String, String>
    {
        match self
        {
            Self::En => &blog.internal_link_list.en,
            Self::De => &blog.internal_link_list.de,
            Self::Eo => &blog.internal_link_list.eo,
        }
    }

    pub fn iter() -> impl Iterator<Item = Self>
    {
        [Self::En, Self::De, Self::Eo].into_iter()
    }

    // This is a weird function, but I think the name is good.
    // TODO: Think of a better one.
    #[allow(clippy::wrong_self_convention)]
    // More readable that way.
    #[allow(clippy::match_same_arms)]
    pub const fn from_relative(self, index: RelativeLang) -> Self
    {
        match (self, index)
        {
            (lang, RelativeLang::Same) => lang,

            (Self::En, RelativeLang::Other1) => Self::De,
            (Self::En, RelativeLang::Other2) => Self::Eo,

            (Self::De, RelativeLang::Other1) => Self::En,
            (Self::De, RelativeLang::Other2) => Self::Eo,

            (Self::Eo, RelativeLang::Other1) => Self::En,
            (Self::Eo, RelativeLang::Other2) => Self::De,
        }
    }

    pub const fn to_index(self) -> usize
    {
        match self
        {
            Self::En => 0,
            Self::De => 1,
            Self::Eo => 2,
        }
    }
}
