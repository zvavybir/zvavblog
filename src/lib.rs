/*
    zvavblog – Static site generator for blog.zvavybir.eu
    Copyright (C) 2023  Matthias Kaak

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License
    as published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#![warn(
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo_common_metadata
)]
// Anachronism
#![allow(clippy::non_ascii_literal)]
// More or less manual checked and documentation agrees with me that
// it's usually not needed.
#![allow(
    clippy::cast_possible_truncation,
    clippy::cast_sign_loss,
    clippy::cast_precision_loss,
    clippy::cast_lossless
)]
// Explicitly decided against; I think `let _ = …` is better than
// `mem::drop(…)`. TODO: align my opinion and community's one with
// each other.
#![allow(let_underscore_drop)]
// I think using default() is actually easier to understand.
#![allow(clippy::default_constructed_unit_structs)]
// Due to the "'_en' – '_eo'" thing this is sadly necessary.
#![allow(clippy::similar_names)]
// Pedantic lint and I think it's wrong.
#![allow(clippy::module_name_repetitions)]
#![feature(iter_intersperse)]

mod gen_atom;
mod gen_html;
mod gen_zip;
mod helpers;
mod lang;
mod parse;
mod types;
mod write;

pub use write::gen_all;
