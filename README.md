# zvavblog
`zvavblog` is the static site generator I wrote for my [blog](https://blog.zvavybir.eu).  Because of [ethical reasons](https://blog.zvavybir.eu/language_tiers.html), `zvavblog` supports English, German, and Esperanto.

This isn't really intended to be used by others, because the supported languages and other things are very directly geared towards me.  Still you are of course [free](https://codeberg.org/zvavybir/zvavblog/src/branch/main/LICENSE.md) to use it yourself.

## Contributing
If you want to contribute, feel free to open an [issue](https://codeberg.org/zvavybir/zvavblog/issues/new) or fill an [PR](https://codeberg.org/zvavybir/zvavblog/pulls).
## License
`zvavblog` is released under the Gnu Affero General Public License version 3 or (at you option) any later version.